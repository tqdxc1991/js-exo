// Exercice 1 Faire une fonction qui retourne true.

export function returnTrue(){
    document.getElementById('function1').innerHTML = "true";
    console.log(true);
}

// Exercice 2 Faire une fonction qui prend en paramètre une chaine de caractères et qui retourne cette même chaine.

export function returnChar(b){
    document.getElementById('function2').innerHTML = b;
        console.log(b);
}

// Exercice 3 Faire une fonction qui prend en paramètre deux chaines de caractères et qui renvoit la concaténation de ces deux chaines.

export function concat(a,b){
    let result;
    
    result = a + b;
    document.getElementById('function3').innerHTML = result;

     console.log(a+b); 
}

// Exercice 4 Faire une fonction qui prend en paramètre deux nombres. La fonction doit retourner :

//     Le premier nombre est plus grand si le premier nombre est plus grand que le deuxième
//     Le premier nombre est plus petit si le premier nombre est plus petit que le deuxième
//     Les deux nombres sont identiques si les deux nombres sont égaux

export function compareNum(a,b){
    let result;
    if(a>b){ result = "Le premier nombre est plus grand. ";}
    else if(a<b){result = "Le premier nombre est plus petit. ";}
    else{result = "Les deux nombres sont identiques.";} 
    document.getElementById('function4').innerHTML = result;
    console.log(result);
}

// Exercice 5 Faire une fonction qui prend en paramètre un nombre et une chaine de caractères et qui renvoit la concaténation de ces deux paramètres.

export function concatNumChar(num,char){
    
    document.getElementById('function5').innerHTML = num+char;
    console.log(num+char);
}

// Exercice 6 Faire une fonction qui prend trois paramètres : nom, prenom et age. Elle doit renvoyer une chaine de la forme : "Bonjour" + nom + prenom + ", tu as " + age + "ans".

export function direBonjour(nom,prenom,age){
    let result;
    result = "Bonjour" + nom + prenom + ", tu as " + age + "ans.";
    document.getElementById('function6').innerHTML = result;
    console.log(result);
}

// Exercice 7 Faire une fonction qui prend deux paramètres : age et genre. Le paramètre genre peut prendre comme valeur Homme ou Femme. La fonction doit renvoyer en fonction des paramètres (gérer tous les cas) :

//     Vous êtes un homme et vous êtes majeur
//     Vous êtes un homme et vous êtes mineur
//     Vous êtes une femme et vous êtes majeur
//     Vous êtes une femme et vous êtes mineur

export function genre(age,genre){
    let result;

    if(age>=18 & genre=="homme"){
        result= "Vous êtes un homme et vous êtes majeur";
    }                    
         else if(age<18 & genre=="homme"){
            result= "Vous êtes un homme et vous êtes mineur";
         }   
         else if(age>=18 & genre=="femme"){
            result= "Vous êtes une femme et vous êtes majeur";
         }
         else if(age<18 & genre=="femme"){
            result= "Vous êtes une femme et vous êtes mineur";
         }
         else{
            return;
         }    
         document.getElementById('function7').innerHTML = result;
         console.log(result);      
}


// Exercice 8 Faire une fonction qui prend en paramètre trois nombres et qui renvoit la somme de ces nombres. Tous les paramètres doivent avoir une valeur par défaut.

export function sum(a =88 ,b = 20,c = 12){
    document.getElementById('function8').innerHTML = a+b+c;
    console.log(a+b+c);
}