// Exercice 1 Créer une variable et l'initialiser à 0. Tant que cette variable n'atteint pas 10 :

//     l'afficher
//     incrémenter de 1
 export function showLessThanTen(){
    let i = 0;
    for(i=0;i<10;i++){
        let result = document.getElementById('exo1').insertAdjacentHTML('beforebegin',i+" |");
    }
}

// Exercice 2 Créer deux variables. Initialiser la première à 0 et la deuxième avec un nombre compris en 1 et 100. Tant que la première variable n'est pas supérieur à 20 :

//     multiplier la première variable avec la deuxième
//     afficher le résultat
//     incrémenter la première variable


  export 	function multiply(){
           let variable1 = 0;
           let variable2 = 50;
           for(variable1=0;variable1<20;variable1++){
            let result2 = document.getElementById('exo2').insertAdjacentHTML('beforebegin',variable1*variable2+" |");
           }
           
       }

//     Exercice 3 Créer deux variables. Initialiser la première à 100 et la deuxième avec un nombre compris en 1 et 100. Tant que la première variable n'est pas inférieur ou égale à 10 :

// multiplier la première variable avec la deuxième
// afficher le résultat
// décrémenter la première variable
export 	function multiplyDecre(){
    let variable1 = 100;
    let variable2 = 50;
    for(variable1=100;variable1>10;variable1--){
     let result3 = document.getElementById('exo3').insertAdjacentHTML('beforebegin',variable1*variable2+" |");
    }
    
}

// Exercice 4 Créer une variable et l'initialiser à 1. Tant que cette variable n'atteint pas 10 :

//     l'afficher
//     l'incrementer de la moitié de sa valeur
export function incrementMoitie(){
    let i = 1;
while (i < 10) { 
     document.getElementById('exo4').insertAdjacentHTML('beforebegin',i+" |");
        i = i + i/2 ;}
}


// Exercice 5 En allant de 1 à 15 avec un pas de 1, afficher le message On y arrive presque...
export function onArrive(){
    let i = 1;
while (i <= 15) { 
     document.getElementById('exo5').insertAdjacentHTML('beforebegin',i+"On y arrive presque... |");
        i ++ ;}
}

// Exercice 6 En allant de 20 à 0 avec un pas de 1, afficher le message C'est presque bon...
export function presqueBon(){
    let i = 20;
while (i >= 0) { 
     document.getElementById('exo6').insertAdjacentHTML('beforebegin',i+"C'est presque bon... |");
        i -- ;}
}


// Exercice 7 En allant de 1 à 100 avec un pas de 15, afficher le message On tient le bon bout...
export function bonBout(){
    let i = 1;
while (i <= 100) { 
     document.getElementById('exo7').insertAdjacentHTML('beforebegin',i+"C'est presque bon... |");
        i = i + 15 ;}
}


// Exercice 8 En allant de 200 à 0 avec un pas de 12, afficher le message Enfin ! ! !
export function enFin(){
    let i = 200;
while (i >= 0) { 
     document.getElementById('exo8').insertAdjacentHTML('beforebegin',i+"Enfin ! ! ! |");
        i = i - 12 ;}
}
