// modules are defined as an array
// [ module function, map of requires ]
//
// map of requires is short require name -> numeric require
//
// anything defined in a previous bundle is accessed via the
// orig method which is the require for previous bundles
parcelRequire = (function (modules, cache, entry, globalName) {
  // Save the require from previous bundle to this closure if any
  var previousRequire = typeof parcelRequire === 'function' && parcelRequire;
  var nodeRequire = typeof require === 'function' && require;

  function newRequire(name, jumped) {
    if (!cache[name]) {
      if (!modules[name]) {
        // if we cannot find the module within our internal map or
        // cache jump to the current global require ie. the last bundle
        // that was added to the page.
        var currentRequire = typeof parcelRequire === 'function' && parcelRequire;
        if (!jumped && currentRequire) {
          return currentRequire(name, true);
        }

        // If there are other bundles on this page the require from the
        // previous one is saved to 'previousRequire'. Repeat this as
        // many times as there are bundles until the module is found or
        // we exhaust the require chain.
        if (previousRequire) {
          return previousRequire(name, true);
        }

        // Try the node require function if it exists.
        if (nodeRequire && typeof name === 'string') {
          return nodeRequire(name);
        }

        var err = new Error('Cannot find module \'' + name + '\'');
        err.code = 'MODULE_NOT_FOUND';
        throw err;
      }

      localRequire.resolve = resolve;
      localRequire.cache = {};

      var module = cache[name] = new newRequire.Module(name);

      modules[name][0].call(module.exports, localRequire, module, module.exports, this);
    }

    return cache[name].exports;

    function localRequire(x){
      return newRequire(localRequire.resolve(x));
    }

    function resolve(x){
      return modules[name][1][x] || x;
    }
  }

  function Module(moduleName) {
    this.id = moduleName;
    this.bundle = newRequire;
    this.exports = {};
  }

  newRequire.isParcelRequire = true;
  newRequire.Module = Module;
  newRequire.modules = modules;
  newRequire.cache = cache;
  newRequire.parent = previousRequire;
  newRequire.register = function (id, exports) {
    modules[id] = [function (require, module) {
      module.exports = exports;
    }, {}];
  };

  var error;
  for (var i = 0; i < entry.length; i++) {
    try {
      newRequire(entry[i]);
    } catch (e) {
      // Save first error but execute all entries
      if (!error) {
        error = e;
      }
    }
  }

  if (entry.length) {
    // Expose entry point to Node, AMD or browser globals
    // Based on https://github.com/ForbesLindesay/umd/blob/master/template.js
    var mainExports = newRequire(entry[entry.length - 1]);

    // CommonJS
    if (typeof exports === "object" && typeof module !== "undefined") {
      module.exports = mainExports;

    // RequireJS
    } else if (typeof define === "function" && define.amd) {
     define(function () {
       return mainExports;
     });

    // <script>
    } else if (globalName) {
      this[globalName] = mainExports;
    }
  }

  // Override the current require with this new one
  parcelRequire = newRequire;

  if (error) {
    // throw error from earlier, _after updating parcelRequire_
    throw error;
  }

  return newRequire;
})({"excercices/exo-1.js":[function(require,module,exports) {
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.showLessThanTen = showLessThanTen;
exports.multiply = multiply;
exports.multiplyDecre = multiplyDecre;
exports.incrementMoitie = incrementMoitie;
exports.onArrive = onArrive;
exports.presqueBon = presqueBon;
exports.bonBout = bonBout;
exports.enFin = enFin;

// Exercice 1 Créer une variable et l'initialiser à 0. Tant que cette variable n'atteint pas 10 :
//     l'afficher
//     incrémenter de 1
function showLessThanTen() {
  var i = 0;

  for (i = 0; i < 10; i++) {
    var result = document.getElementById('exo1').insertAdjacentHTML('beforebegin', i + " |");
  }
} // Exercice 2 Créer deux variables. Initialiser la première à 0 et la deuxième avec un nombre compris en 1 et 100. Tant que la première variable n'est pas supérieur à 20 :
//     multiplier la première variable avec la deuxième
//     afficher le résultat
//     incrémenter la première variable


function multiply() {
  var variable1 = 0;
  var variable2 = 50;

  for (variable1 = 0; variable1 < 20; variable1++) {
    var result2 = document.getElementById('exo2').insertAdjacentHTML('beforebegin', variable1 * variable2 + " |");
  }
} //     Exercice 3 Créer deux variables. Initialiser la première à 100 et la deuxième avec un nombre compris en 1 et 100. Tant que la première variable n'est pas inférieur ou égale à 10 :
// multiplier la première variable avec la deuxième
// afficher le résultat
// décrémenter la première variable


function multiplyDecre() {
  var variable1 = 100;
  var variable2 = 50;

  for (variable1 = 100; variable1 > 10; variable1--) {
    var result3 = document.getElementById('exo3').insertAdjacentHTML('beforebegin', variable1 * variable2 + " |");
  }
} // Exercice 4 Créer une variable et l'initialiser à 1. Tant que cette variable n'atteint pas 10 :
//     l'afficher
//     l'incrementer de la moitié de sa valeur


function incrementMoitie() {
  var i = 1;

  while (i < 10) {
    document.getElementById('exo4').insertAdjacentHTML('beforebegin', i + " |");
    i = i + i / 2;
  }
} // Exercice 5 En allant de 1 à 15 avec un pas de 1, afficher le message On y arrive presque...


function onArrive() {
  var i = 1;

  while (i <= 15) {
    document.getElementById('exo5').insertAdjacentHTML('beforebegin', i + "On y arrive presque... |");
    i++;
  }
} // Exercice 6 En allant de 20 à 0 avec un pas de 1, afficher le message C'est presque bon...


function presqueBon() {
  var i = 20;

  while (i >= 0) {
    document.getElementById('exo6').insertAdjacentHTML('beforebegin', i + "C'est presque bon... |");
    i--;
  }
} // Exercice 7 En allant de 1 à 100 avec un pas de 15, afficher le message On tient le bon bout...


function bonBout() {
  var i = 1;

  while (i <= 100) {
    document.getElementById('exo7').insertAdjacentHTML('beforebegin', i + "C'est presque bon... |");
    i = i + 15;
  }
} // Exercice 8 En allant de 200 à 0 avec un pas de 12, afficher le message Enfin ! ! !


function enFin() {
  var i = 200;

  while (i >= 0) {
    document.getElementById('exo8').insertAdjacentHTML('beforebegin', i + "Enfin ! ! ! |");
    i = i - 12;
  }
}
},{}],"excercices/exo-2.js":[function(require,module,exports) {
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.returnTrue = returnTrue;
exports.returnChar = returnChar;
exports.concat = concat;
exports.compareNum = compareNum;
exports.concatNumChar = concatNumChar;
exports.direBonjour = direBonjour;
exports.genre = genre;
exports.sum = sum;

// Exercice 1 Faire une fonction qui retourne true.
function returnTrue() {
  document.getElementById('function1').innerHTML = "true";
  console.log(true);
} // Exercice 2 Faire une fonction qui prend en paramètre une chaine de caractères et qui retourne cette même chaine.


function returnChar(b) {
  document.getElementById('function2').innerHTML = b;
  console.log(b);
} // Exercice 3 Faire une fonction qui prend en paramètre deux chaines de caractères et qui renvoit la concaténation de ces deux chaines.


function concat(a, b) {
  var result;
  result = a + b;
  document.getElementById('function3').innerHTML = result;
  console.log(a + b);
} // Exercice 4 Faire une fonction qui prend en paramètre deux nombres. La fonction doit retourner :
//     Le premier nombre est plus grand si le premier nombre est plus grand que le deuxième
//     Le premier nombre est plus petit si le premier nombre est plus petit que le deuxième
//     Les deux nombres sont identiques si les deux nombres sont égaux


function compareNum(a, b) {
  var result;

  if (a > b) {
    result = "Le premier nombre est plus grand. ";
  } else if (a < b) {
    result = "Le premier nombre est plus petit. ";
  } else {
    result = "Les deux nombres sont identiques.";
  }

  document.getElementById('function4').innerHTML = result;
  console.log(result);
} // Exercice 5 Faire une fonction qui prend en paramètre un nombre et une chaine de caractères et qui renvoit la concaténation de ces deux paramètres.


function concatNumChar(num, char) {
  document.getElementById('function5').innerHTML = num + char;
  console.log(num + char);
} // Exercice 6 Faire une fonction qui prend trois paramètres : nom, prenom et age. Elle doit renvoyer une chaine de la forme : "Bonjour" + nom + prenom + ", tu as " + age + "ans".


function direBonjour(nom, prenom, age) {
  var result;
  result = "Bonjour" + nom + prenom + ", tu as " + age + "ans.";
  document.getElementById('function6').innerHTML = result;
  console.log(result);
} // Exercice 7 Faire une fonction qui prend deux paramètres : age et genre. Le paramètre genre peut prendre comme valeur Homme ou Femme. La fonction doit renvoyer en fonction des paramètres (gérer tous les cas) :
//     Vous êtes un homme et vous êtes majeur
//     Vous êtes un homme et vous êtes mineur
//     Vous êtes une femme et vous êtes majeur
//     Vous êtes une femme et vous êtes mineur


function genre(age, genre) {
  var result;

  if (age >= 18 & genre == "homme") {
    result = "Vous êtes un homme et vous êtes majeur";
  } else if (age < 18 & genre == "homme") {
    result = "Vous êtes un homme et vous êtes mineur";
  } else if (age >= 18 & genre == "femme") {
    result = "Vous êtes une femme et vous êtes majeur";
  } else if (age < 18 & genre == "femme") {
    result = "Vous êtes une femme et vous êtes mineur";
  } else {
    return;
  }

  document.getElementById('function7').innerHTML = result;
  console.log(result);
} // Exercice 8 Faire une fonction qui prend en paramètre trois nombres et qui renvoit la somme de ces nombres. Tous les paramètres doivent avoir une valeur par défaut.


function sum() {
  var a = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 88;
  var b = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 20;
  var c = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 12;
  document.getElementById('function8').innerHTML = a + b + c;
  console.log(a + b + c);
}
},{}],"excercices/exo-3.js":[function(require,module,exports) {
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.department = exports.mois = void 0;
// Exercice 1 Créer un tableau mois et l'initialiser avec le nom des douze mois de l'année.
var mois = ["janver", "fevrier", "mars", "avil", "mai", "juin", "juillet", "aout", "septembre", "octobre", "novembre", "decembre"]; // Exercice 5 Créer un tableau associatif avec comme index le numéro des départements des Hauts de France et en valeur leur nom.

exports.mois = mois;
var department = {
  2: "Aisne",
  59: "Nord",
  60: "Oise",
  62: "Pas-de-Calais",
  80: "Somme"
};
exports.department = department;
},{}],"main.js":[function(require,module,exports) {
"use strict";

var _exo = require("./excercices/exo-1.js");

var _exo2 = require("./excercices/exo-2.js");

var _exo3 = require("./excercices/exo-3.js");

//boucles
document.getElementById('btn1').addEventListener('click', function () {
  (0, _exo.showLessThanTen)();
});
document.getElementById('btn2').addEventListener('click', function () {
  (0, _exo.multiply)();
});
document.getElementById('btn3').addEventListener('click', function () {
  (0, _exo.multiplyDecre)();
});
document.getElementById('btn4').addEventListener('click', function () {
  (0, _exo.incrementMoitie)();
});
document.getElementById('btn5').addEventListener('click', function () {
  (0, _exo.onArrive)();
});
document.getElementById('btn6').addEventListener('click', function () {
  (0, _exo.presqueBon)();
});
document.getElementById('btn7').addEventListener('click', function () {
  (0, _exo.bonBout)();
});
document.getElementById('btn8').addEventListener('click', function () {
  (0, _exo.enFin)();
}); //functions

(0, _exo2.returnTrue)();
(0, _exo2.returnChar)("Qiong");
(0, _exo2.concat)("TANG", " Qiong");
(0, _exo2.compareNum)(100, 100);
(0, _exo2.concatNumChar)("Simplon", 2020);
(0, _exo2.direBonjour)("TANG", "Qiong", 29);
(0, _exo2.genre)(29, "femme");
(0, _exo2.sum)(); //tableaux

document.getElementById('tableaux1').innerHTML = _exo3.mois;
console.log(_exo3.mois); // Exercice 2 Avec le tableau de l'exercice 1, afficher la valeur de la troisième ligne de ce tableau.

document.getElementById('tableaux2').innerHTML = _exo3.mois[2];
console.log(_exo3.mois[2]); // Exercice 3 Avec le tableau de l'exercice , afficher la valeur de l'index 5.

document.getElementById('tableaux3').innerHTML = _exo3.mois[5];
console.log(_exo3.mois[5]); // Exercice 4 Avec le tableau de l'exercice 1, modifier le mois de aout pour lui ajouter l'accent manquant.

_exo3.mois[7] = "août";
document.getElementById('tableaux4').innerHTML = _exo3.mois[7];
console.log(_exo3.mois[7]); //exercice 5

var str = JSON.stringify(_exo3.department);
console.log(str);
document.getElementById('tableaux5').innerHTML = str; // Exercice 6 Avec le tableau de l'exercice 5, afficher la valeur de l'index 59.

document.getElementById('tableaux6').innerHTML = _exo3.department[59];
console.log(_exo3.department[59]); // Exercice 7 Avec le tableau de l'exercice 5, ajouter la ligne correspondant au département de la ville de Reims.

_exo3.department[51] = "Reims";
var str2 = JSON.stringify(_exo3.department);
console.log(str2);
document.getElementById('tableaux7').innerHTML = str2; // Exercice 8 Avec le tableau de l'exercice 1 et une boucle, afficher toutes les valeurs de ce tableau.

var txt = "";

_exo3.mois.forEach(function (value) {
  console.log(value);
  txt = txt + value + "<br>";
});

document.getElementById('tableaux8').innerHTML = txt; // Exercice 9 Avec le tableau de l'exercice 5, afficher toutes les valeurs de ce tableau.

var myArray = Object.values(_exo3.department);
document.getElementById("tableaux9").innerHTML = myArray;
console.log(myArray); // Exercice 10 Avec le tableau de l'exercice 5, afficher toutes 
// les valeurs de ce tableau ainsi que les clés associés. Cela pourra être, par exemple, de la
//  forme : "Le département" + nom_departement + "a le numéro" + num_departement

var x,
    text = "";

for (x in _exo3.department) {
  text += "Le département :" + _exo3.department[x] + " a le numéro :" + x + "<br>";
  console.log(text);
}

;
document.getElementById("tableaux9").innerHTML = text;
},{"./excercices/exo-1.js":"excercices/exo-1.js","./excercices/exo-2.js":"excercices/exo-2.js","./excercices/exo-3.js":"excercices/exo-3.js"}],"../../../.npm/_npx/10177/lib/node_modules/parcel/src/builtins/hmr-runtime.js":[function(require,module,exports) {
var global = arguments[3];
var OVERLAY_ID = '__parcel__error__overlay__';
var OldModule = module.bundle.Module;

function Module(moduleName) {
  OldModule.call(this, moduleName);
  this.hot = {
    data: module.bundle.hotData,
    _acceptCallbacks: [],
    _disposeCallbacks: [],
    accept: function (fn) {
      this._acceptCallbacks.push(fn || function () {});
    },
    dispose: function (fn) {
      this._disposeCallbacks.push(fn);
    }
  };
  module.bundle.hotData = null;
}

module.bundle.Module = Module;
var checkedAssets, assetsToAccept;
var parent = module.bundle.parent;

if ((!parent || !parent.isParcelRequire) && typeof WebSocket !== 'undefined') {
  var hostname = "" || location.hostname;
  var protocol = location.protocol === 'https:' ? 'wss' : 'ws';
  var ws = new WebSocket(protocol + '://' + hostname + ':' + "36729" + '/');

  ws.onmessage = function (event) {
    checkedAssets = {};
    assetsToAccept = [];
    var data = JSON.parse(event.data);

    if (data.type === 'update') {
      var handled = false;
      data.assets.forEach(function (asset) {
        if (!asset.isNew) {
          var didAccept = hmrAcceptCheck(global.parcelRequire, asset.id);

          if (didAccept) {
            handled = true;
          }
        }
      }); // Enable HMR for CSS by default.

      handled = handled || data.assets.every(function (asset) {
        return asset.type === 'css' && asset.generated.js;
      });

      if (handled) {
        console.clear();
        data.assets.forEach(function (asset) {
          hmrApply(global.parcelRequire, asset);
        });
        assetsToAccept.forEach(function (v) {
          hmrAcceptRun(v[0], v[1]);
        });
      } else if (location.reload) {
        // `location` global exists in a web worker context but lacks `.reload()` function.
        location.reload();
      }
    }

    if (data.type === 'reload') {
      ws.close();

      ws.onclose = function () {
        location.reload();
      };
    }

    if (data.type === 'error-resolved') {
      console.log('[parcel] ✨ Error resolved');
      removeErrorOverlay();
    }

    if (data.type === 'error') {
      console.error('[parcel] 🚨  ' + data.error.message + '\n' + data.error.stack);
      removeErrorOverlay();
      var overlay = createErrorOverlay(data);
      document.body.appendChild(overlay);
    }
  };
}

function removeErrorOverlay() {
  var overlay = document.getElementById(OVERLAY_ID);

  if (overlay) {
    overlay.remove();
  }
}

function createErrorOverlay(data) {
  var overlay = document.createElement('div');
  overlay.id = OVERLAY_ID; // html encode message and stack trace

  var message = document.createElement('div');
  var stackTrace = document.createElement('pre');
  message.innerText = data.error.message;
  stackTrace.innerText = data.error.stack;
  overlay.innerHTML = '<div style="background: black; font-size: 16px; color: white; position: fixed; height: 100%; width: 100%; top: 0px; left: 0px; padding: 30px; opacity: 0.85; font-family: Menlo, Consolas, monospace; z-index: 9999;">' + '<span style="background: red; padding: 2px 4px; border-radius: 2px;">ERROR</span>' + '<span style="top: 2px; margin-left: 5px; position: relative;">🚨</span>' + '<div style="font-size: 18px; font-weight: bold; margin-top: 20px;">' + message.innerHTML + '</div>' + '<pre>' + stackTrace.innerHTML + '</pre>' + '</div>';
  return overlay;
}

function getParents(bundle, id) {
  var modules = bundle.modules;

  if (!modules) {
    return [];
  }

  var parents = [];
  var k, d, dep;

  for (k in modules) {
    for (d in modules[k][1]) {
      dep = modules[k][1][d];

      if (dep === id || Array.isArray(dep) && dep[dep.length - 1] === id) {
        parents.push(k);
      }
    }
  }

  if (bundle.parent) {
    parents = parents.concat(getParents(bundle.parent, id));
  }

  return parents;
}

function hmrApply(bundle, asset) {
  var modules = bundle.modules;

  if (!modules) {
    return;
  }

  if (modules[asset.id] || !bundle.parent) {
    var fn = new Function('require', 'module', 'exports', asset.generated.js);
    asset.isNew = !modules[asset.id];
    modules[asset.id] = [fn, asset.deps];
  } else if (bundle.parent) {
    hmrApply(bundle.parent, asset);
  }
}

function hmrAcceptCheck(bundle, id) {
  var modules = bundle.modules;

  if (!modules) {
    return;
  }

  if (!modules[id] && bundle.parent) {
    return hmrAcceptCheck(bundle.parent, id);
  }

  if (checkedAssets[id]) {
    return;
  }

  checkedAssets[id] = true;
  var cached = bundle.cache[id];
  assetsToAccept.push([bundle, id]);

  if (cached && cached.hot && cached.hot._acceptCallbacks.length) {
    return true;
  }

  return getParents(global.parcelRequire, id).some(function (id) {
    return hmrAcceptCheck(global.parcelRequire, id);
  });
}

function hmrAcceptRun(bundle, id) {
  var cached = bundle.cache[id];
  bundle.hotData = {};

  if (cached) {
    cached.hot.data = bundle.hotData;
  }

  if (cached && cached.hot && cached.hot._disposeCallbacks.length) {
    cached.hot._disposeCallbacks.forEach(function (cb) {
      cb(bundle.hotData);
    });
  }

  delete bundle.cache[id];
  bundle(id);
  cached = bundle.cache[id];

  if (cached && cached.hot && cached.hot._acceptCallbacks.length) {
    cached.hot._acceptCallbacks.forEach(function (cb) {
      cb();
    });

    return true;
  }
}
},{}]},{},["../../../.npm/_npx/10177/lib/node_modules/parcel/src/builtins/hmr-runtime.js","main.js"], null)
//# sourceMappingURL=dist/main.js.map