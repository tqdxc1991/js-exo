import { showLessThanTen,multiply,multiplyDecre,incrementMoitie,onArrive,presqueBon,bonBout,enFin } from './excercices/exo-1.js';

import { returnTrue,returnChar,concat,compareNum,concatNumChar,direBonjour,genre,sum } from './excercices/exo-2.js';

import { mois,department } from './excercices/exo-3.js'


//boucles

document.getElementById('btn1').addEventListener('click', function() {
    showLessThanTen();
});

document.getElementById('btn2').addEventListener('click', function() {
    multiply();
});



document.getElementById('btn3').addEventListener('click', function() {
    multiplyDecre();
});

document.getElementById('btn4').addEventListener('click', function() {
    incrementMoitie();
});

document.getElementById('btn5').addEventListener('click', function() {
    onArrive();
});

document.getElementById('btn6').addEventListener('click', function() {
    presqueBon();
});


document.getElementById('btn7').addEventListener('click', function() {
    bonBout();
});


document.getElementById('btn8').addEventListener('click', function() {
    enFin();
});





//functions

returnTrue();


returnChar("Qiong");

concat("TANG"," Qiong");

compareNum(100,100);

concatNumChar("Simplon",2020)

direBonjour("TANG","Qiong",29);

genre(29,"femme")

sum();




//tableaux

document.getElementById('tableaux1').innerHTML = mois;

console.log(mois);
// Exercice 2 Avec le tableau de l'exercice 1, afficher la valeur de la troisième ligne de ce tableau.


document.getElementById('tableaux2').innerHTML = mois[2];
console.log(mois[2]);

// Exercice 3 Avec le tableau de l'exercice , afficher la valeur de l'index 5.
document.getElementById('tableaux3').innerHTML = mois[5];
console.log(mois[5]);

// Exercice 4 Avec le tableau de l'exercice 1, modifier le mois de aout pour lui ajouter l'accent manquant.

 
    mois[7] = "août";
    document.getElementById('tableaux4').innerHTML = mois[7];
    console.log(mois[7]);
 
//exercice 5
let str = JSON.stringify(department);
console.log(str);
document.getElementById('tableaux5').innerHTML = str;

// Exercice 6 Avec le tableau de l'exercice 5, afficher la valeur de l'index 59.
document.getElementById('tableaux6').innerHTML = department[59];
console.log(department[59]);

// Exercice 7 Avec le tableau de l'exercice 5, ajouter la ligne correspondant au département de la ville de Reims.
department[51] = "Reims";
let str2 = JSON.stringify(department);
console.log(str2);
document.getElementById('tableaux7').innerHTML = str2;

// Exercice 8 Avec le tableau de l'exercice 1 et une boucle, afficher toutes les valeurs de ce tableau.
let txt ="" ;
mois.forEach(value => {console.log(value);
    txt = txt + value + "<br>";
})

document.getElementById('tableaux8').innerHTML = txt;

// Exercice 9 Avec le tableau de l'exercice 5, afficher toutes les valeurs de ce tableau.
let myArray = Object.values(department);
document.getElementById("tableaux9").innerHTML = myArray;
console.log(myArray);


// Exercice 10 Avec le tableau de l'exercice 5, afficher toutes 
// les valeurs de ce tableau ainsi que les clés associés. Cela pourra être, par exemple, de la
//  forme : "Le département" + nom_departement + "a le numéro" + num_departement



var x, text = "";

for (x in department) {
  text += "Le département :" + department[x] + " a le numéro :" + x + "<br>";
  console.log(text);
};

document.getElementById("tableaux9").innerHTML = text;